﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using RabbitMQModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQModule.Services
{
    public class ElasticServiceAgent
    {
        private Globals globals;
        public async Task<ResultItem<GetRabbitMQBaseConfigResult>> GetBaseConfig()
        {
            var taskResult = await Task.Run<ResultItem<GetRabbitMQBaseConfigResult>>(() =>
            {
                var result = new ResultItem<GetRabbitMQBaseConfigResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new GetRabbitMQBaseConfigResult()
                };

                var searchBaseConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = Config.LocalData.Object_BaseConfig.GUID
                    }
                };

                var dbReaderBaseConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderBaseConfig.GetDataObjects(searchBaseConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the base-config!";
                    return result;
                }

                result.Result.BaseConfig = dbReaderBaseConfig.Objects1.FirstOrDefault();

                if (result.Result.BaseConfig == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No base-config found!";
                    return result;
                }

                var searchServerPort = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.BaseConfig.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_RabbitMQModule_uses_Server_Port.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_RabbitMQModule_uses_Server_Port.ID_Class_Right
                    }
                };

                var dbReaderServerPort = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderServerPort.GetDataObjectRel(searchServerPort);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Server-Url!";
                    return result;
                }

                result.Result.ServerPort = dbReaderServerPort.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).FirstOrDefault();

                if (result.Result.ServerPort == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Server-Port for RabbitMQ configured!";
                    return result;
                }

                var searchServer = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.ServerPort.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_Server_Port_belonging_Source_Server.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Server_Port_belonging_Source_Server.ID_Class_Right
                    }
                };

                var dbReaderServer = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderServer.GetDataObjectRel(searchServer);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Server!";
                    return result;
                }

                result.Result.Server = dbReaderServer.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).FirstOrDefault();

                if (result.Result.Server == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"No Server of Server-Port {result.Result.ServerPort.Name} found!";
                    return result;
                }

                var searchPort = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.ServerPort.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_Server_Port_belonging_Source_Port.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Server_Port_belonging_Source_Port.ID_Class_Right
                    }
                };

                var dbReaderPort = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderPort.GetDataObjectRel(searchPort);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = $"Error while getting the Port of Server-Port {result.Result.ServerPort.Name}!";
                    return result;
                }

                result.Result.PortItem = dbReaderPort.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).FirstOrDefault();

                if (result.Result.PortItem == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"No Port of Server-Port {result.Result.ServerPort.Name} found!";
                    return result;
                }

                var searchUser = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.BaseConfig.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_RabbitMQModule_authorized_by_user.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_RabbitMQModule_authorized_by_user.ID_Class_Right
                    }
                };

                var dbReaderUser = new OntologyModDBConnector(globals);
                result.ResultState = dbReaderUser.GetDataObjectRel(searchUser);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the User!";
                    return result;
                }

                result.Result.User = dbReaderUser.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).FirstOrDefault();

                if (result.Result.User == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No User found!";
                    return result;
                }

                var searchMasterUser = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.BaseConfig.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_RabbitMQModule_masteruser_user.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_RabbitMQModule_masteruser_user.ID_Class_Right
                    }
                };

                var dbReaderMasterUser = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderMasterUser.GetDataObjectRel(searchMasterUser);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the master-user!";
                    return result;
                }

                result.Result.MasterUser = dbReaderMasterUser.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).FirstOrDefault();

                if (result.Result.MasterUser == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Masteruser found!";
                    return result;
                }

                var searchEmailAddress = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = result.Result.MasterUser.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_eMail_Address_belongs_to_user.ID_RelationType,
                        ID_Parent_Object = Config.LocalData.ClassRel_eMail_Address_belongs_to_user.ID_Class_Left
                    }
                };

                var dbReaderEmailAddress = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderEmailAddress.GetDataObjectRel(searchEmailAddress);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Email-Address of Master-User!";
                    return result;
                }

                result.Result.EmailAddress = dbReaderEmailAddress.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Object,
                    Name = rel.Name_Object,
                    GUID_Parent = rel.ID_Parent_Object,
                    Type = globals.Type_Object
                }).FirstOrDefault();

                if (result.Result.EmailAddress == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Email-Address found!";
                    return result;
                }


                var searchQueue = new List<clsObjectRel>{
                    new clsObjectRel
                    {
                        ID_Object = result.Result.BaseConfig.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_RabbitMQModule_uses_Queue__RabbitMQ_.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_RabbitMQModule_uses_Queue__RabbitMQ_.ID_Class_Right
                    }
                };

                var dbReaderQueue = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderQueue.GetDataObjectRel(searchQueue);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Queue!";
                    return result;
                }

                result.Result.Queue = dbReaderQueue.ObjectRels.OrderBy(rel => rel.OrderID).ThenBy(rel => rel.Name_Other).Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).FirstOrDefault();

                if (result.Result.Queue == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Queue found!";
                    return result;
                }

                var searchExchange = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.BaseConfig.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_RabbitMQModule_uses_Exchange__RabbitMQ_.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_RabbitMQModule_uses_Exchange__RabbitMQ_.ID_Class_Right
                    }
                };

                var dbReaderExchange = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderExchange.GetDataObjectRel(searchExchange);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Exchange!";
                    return result;
                }

                result.Result.Exchange = dbReaderExchange.ObjectRels.OrderBy(rel => rel.OrderID).ThenBy(rel => rel.Name_Other).Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).FirstOrDefault();

                if (result.Result.Exchange == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Exchange found!";
                    return result;
                }

                var searchRoutingKey = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.BaseConfig.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_RabbitMQModule_uses_RoutingKey__RabbitMQ_.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_RabbitMQModule_uses_RoutingKey__RabbitMQ_.ID_Class_Right
                    }
                };

                var dbReaderRoutingKey = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderRoutingKey.GetDataObjectRel(searchRoutingKey);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Routing-key!";
                    return result;
                }

                result.Result.RoutingKey = dbReaderRoutingKey.ObjectRels.OrderBy(rel => rel.OrderID).ThenBy(rel => rel.Name_Other).Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).FirstOrDefault();

                if (result.Result.RoutingKey == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No routingkey found!";
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<GetStartModuleMessageModelResult>> GetStartModuleMessageModel(SendStartModuleMessageRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetStartModuleMessageModelResult>>(() =>
            {
                var result = new ResultItem<GetStartModuleMessageModelResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new GetStartModuleMessageModelResult()
                };

                var searchBaseMessage = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig,
                        GUID_Parent = StartModuleMessage.LocalData.Class_Message__Queue_.GUID
                    }
                };

                var dbReaderBaseMessage = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderBaseMessage.GetDataObjects(searchBaseMessage);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Base-Message!";
                    return result;
                }

                result.Result.BaseMessage = dbReaderBaseMessage.Objects1.FirstOrDefault();

                if (result.Result.BaseMessage == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Base-Message found!";
                    return result;
                }

                var searchSubMessages = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.BaseMessage.GUID,
                        ID_RelationType = StartModuleMessage.LocalData.ClassRel_Message__Queue__contains_Message__Queue_.ID_RelationType,
                        ID_Parent_Other = StartModuleMessage.LocalData.ClassRel_Message__Queue__contains_Message__Queue_.ID_Class_Right
                    }
                };

                var dbReaderSubMessages = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderSubMessages.GetDataObjectRel(searchSubMessages);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the sub-messages!";
                    return result;
                }

                result.Result.Messages = dbReaderSubMessages.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).ToList();

                if (!result.Result.Messages.Any())
                {
                    result.Result.Messages.Add(result.Result.BaseMessage);
                }

                var searchStartModelItem = result.Result.Messages.Select(msg => new clsObjectRel
                {
                    ID_Object = msg.GUID,
                    ID_RelationType = StartModuleMessage.LocalData.ClassRel_Message__Queue__uses_ModuleConfig__SyncStarter_.ID_RelationType,
                    ID_Parent_Other = StartModuleMessage.LocalData.ClassRel_Message__Queue__uses_ModuleConfig__SyncStarter_.ID_Class_Right
                }).ToList();

                var dbReaderConfigs = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfigs.GetDataObjectRel(searchStartModelItem);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the start-module configs!";
                    return result;
                }

                result.Result.ModuleConfigs = dbReaderConfigs.ObjectRels;

                if (!result.Result.ModuleConfigs.Any())
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Error while getting the start-module configs!";
                    return result;
                }

                var searchSchedules = result.Result.Messages.Select(msg => new clsObjectRel
                {
                    ID_Object = msg.GUID,
                    ID_RelationType = StartModuleMessage.LocalData.ClassRel_Message__Queue__uses_Scheduled_Job.ID_RelationType,
                    ID_Parent_Other = StartModuleMessage.LocalData.ClassRel_Message__Queue__uses_Scheduled_Job.ID_Class_Right
                }).ToList();

                var dbReaderSchedules = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderSchedules.GetDataObjectRel(searchSchedules);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Schedules!";
                    return result;
                }

                result.Result.ScheduledJobs = dbReaderSchedules.ObjectRels;

                if (!result.Result.ScheduledJobs.Any())
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Scheduled jobs found!";
                    return result;
                }

                var searchExchange = result.Result.Messages.Select(msg => new clsObjectRel
                {
                    ID_Object = msg.GUID,
                    ID_RelationType = StartModuleMessage.LocalData.ClassRel_Message__Queue__uses_Exchange__RabbitMQ_.ID_RelationType,
                    ID_Parent_Other = StartModuleMessage.LocalData.ClassRel_Message__Queue__uses_Exchange__RabbitMQ_.ID_Class_Right
                }).ToList();

                var dbReaderExchanges = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderExchanges.GetDataObjectRel(searchExchange);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Exchanges!";
                    return result;
                }

                result.Result.Exchanges = dbReaderExchanges.ObjectRels;

                if (!result.Result.Exchanges.Any())
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Exchanges found!";
                    return result;
                }

                var searchQueues = result.Result.Messages.Select(msg => new clsObjectRel
                {
                    ID_Object = msg.GUID,
                    ID_RelationType = StartModuleMessage.LocalData.ClassRel_Message__Queue__uses_Queue__RabbitMQ_.ID_RelationType,
                    ID_Parent_Other = StartModuleMessage.LocalData.ClassRel_Message__Queue__uses_Queue__RabbitMQ_.ID_Class_Right
                }).ToList();

                var dbReaderQueues = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderQueues.GetDataObjectRel(searchExchange);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Queues!";
                    return result;
                }

                result.Result.Queues = dbReaderQueues.ObjectRels;

                if (!result.Result.Queues.Any())
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Queues found!";
                    return result;
                }

                var searchRoutingKeys = result.Result.Messages.Select(msg => new clsObjectRel
                {
                    ID_Object = msg.GUID,
                    ID_RelationType = StartModuleMessage.LocalData.ClassRel_Message__Queue__uses_RoutingKey__RabbitMQ_.ID_RelationType,
                    ID_Parent_Other = StartModuleMessage.LocalData.ClassRel_Message__Queue__uses_RoutingKey__RabbitMQ_.ID_Class_Right
                }).ToList();

                var dbReaderRoutingKeys = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderRoutingKeys.GetDataObjectRel(searchRoutingKeys);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the routingkeys!";
                    return result;
                }

                result.Result.RoutingKeys = dbReaderRoutingKeys.ObjectRels;

                if (!result.Result.RoutingKeys.Any())
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Routingkeys found!";
                    return result;
                }

                return result;
            });
            return taskResult;
        }

        public ElasticServiceAgent(Globals globals)
        {
            this.globals = globals;
        }
    }
}

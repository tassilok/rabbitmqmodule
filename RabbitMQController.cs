﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQModule.Models;
using RabbitMQModule.Services;
using SecurityModule;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace RabbitMQModule
{
    
    public class RabbitMQController : AppController
    {
        public async Task<clsOntologyItem> CloseRabbitMQConnection(RabbitMQConnection connection)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = Globals.LState_Success.Clone();

                try
                {
                    connection.Channel.Close();
                    connection.Connection.Close();
                }
                catch (Exception ex)
                {
                    result = Globals.LState_Error.Clone();
                    result.Additional1 = ex.Message;
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public clsOntologyItem OpenConnection(RabbitMQConnection connection)
        {
            var result = Globals.LState_Success.Clone();

            try
            {
                connection.Connection = connection.ConnectionFactory.CreateConnection();
                connection.Channel = connection.Connection.CreateModel();
            }
            catch (Exception ex)
            {
                result = Globals.LState_Error.Clone();
                result.Additional1 = $"Error while connecting: {ex.Message}";
                
            }

            return result;
        }


        public clsOntologyItem InitializeStartModuleHandler(RabbitMQConnection connection)
        {
            var result = Globals.LState_Success.Clone();

            if (connection.Consumer != null)
            {
                result = Globals.LState_Error.Clone();
                result.Additional1 = "There is a consumer!";
                return result;
            }
            
            try
            {
                connection.Consumer = new EventingBasicConsumer(connection.Channel);

                result = connection.CheckQueue();
                //if (result.GUID == Globals.LState_Error.GUID)
                //{
                //    return result;
                //}

                return result;
            }
            catch (Exception ex)
            {
                result = Globals.LState_Error.Clone();
                result.Additional1 = $"Error while trying to attach handerl: {ex.Message}";
                return result;
            }
            
        }

        public async Task<ResultItem<RabbitMQConnection>> ConnectToRabbitMQ(ConnectionRequest request)
        {
            var taskResult = await Task.Run<ResultItem<RabbitMQConnection>>(async() =>
           {
               var result = new ResultItem<RabbitMQConnection>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new RabbitMQConnection(Globals)
               };

               var elasticAgent = new ElasticServiceAgent(Globals);

               request.MessageOutput?.OutputInfo("Get BaseConfig...");
               var getBaseConfigResult = await elasticAgent.GetBaseConfig();
               request.MessageOutput?.OutputInfo("Have BaseConfig.");

               result.ResultState = getBaseConfigResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               var securityController = new SecurityController(Globals);

               request.MessageOutput?.OutputInfo($"Login with Email-Address {getBaseConfigResult.Result.EmailAddress.Name}...");
               var loginResult = await securityController.Login(getBaseConfigResult.Result.EmailAddress.Name, request.MasterPassword);

               result.ResultState = loginResult.Result;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while login!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo($"Login with Email-Address {getBaseConfigResult.Result.EmailAddress.Name} successful.");

               request.MessageOutput?.OutputInfo($"Get Password of {loginResult.Result.Name}...");

               var passwordResult = await securityController.GetPassword(getBaseConfigResult.Result.User, request.MasterPassword);

               result.ResultState = passwordResult.Result;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the password!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo($"Get Password of {loginResult.Result.Name} successful");

               var credentialItem = passwordResult.CredentialItems.FirstOrDefault();

               if (credentialItem == null)
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = $"No password of {loginResult.Result.Name} found!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               result.Result.ConnectionFactory = new ConnectionFactory();
               // "guest"/"guest" by default, limited to localhost connections
               result.Result.ConnectionFactory.UserName = getBaseConfigResult.Result.User.Name;
               result.Result.ConnectionFactory.Password = credentialItem.Password.Name_Other;
               result.Result.ConnectionFactory.VirtualHost = "/";
               result.Result.ConnectionFactory.HostName = getBaseConfigResult.Result.Server.Name;
               result.Result.ConnectionFactory.Port = getBaseConfigResult.Result.Port;
               result.Result.ConnectionFactory.AutomaticRecoveryEnabled = true;
               result.Result.Queue = getBaseConfigResult.Result.Queue;
               result.Result.Exchange = getBaseConfigResult.Result.Exchange;
               result.Result.RoutingKey = getBaseConfigResult.Result.RoutingKey;

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<RabbitMQConnection>> CreateQueueAndExchange(CreateQueueAndExchangeRequest request)
        {
            var taskResult = await Task.Run<ResultItem<RabbitMQConnection>>(async() =>
            {
                var result = new ResultItem<RabbitMQConnection>
                {
                    ResultState = Globals.LState_Success.Clone()
                };

                var connectionRequest = new ConnectionRequest(request.PasswordMasterUser)
                {
                    MessageOutput = request.MessageOutput
                };


                RabbitMQConnection connection = request.Connection;

                if (connection == null)
                {
                    var connectionResult = await ConnectToRabbitMQ(connectionRequest);

                    result.ResultState = connectionResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }
                    connection = connectionResult.Result;
                }

                var queuDeclareOk = connection.Channel.QueueDeclare(connection.Queue.Name, false, false, false, null);
                connection.Channel.ExchangeDeclare(connection.Exchange.Name, ExchangeType.Direct);
                    

                result.Result = connection;
               
                connection.Channel.QueueBind(connection.Queue.Name, connection.Exchange.Name, connection.RoutingKey.Name);

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SendStartModuleMessage(SendStartModuleMessageRequest request)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var result = Globals.LState_Success.Clone();

                var elasticAgent = new ElasticServiceAgent(Globals);

                request.MessageOutput?.OutputInfo("Get Messages...");
                var getMessagesResult = await elasticAgent.GetStartModuleMessageModel(request);

                result = getMessagesResult.ResultState;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

                var messages = (from message in getMessagesResult.Result.Messages
                                join config in getMessagesResult.Result.ModuleConfigs on message.GUID equals config.ID_Object
                                join scheduler in getMessagesResult.Result.ScheduledJobs on message.GUID equals scheduler.ID_Object
                                join exchange in getMessagesResult.Result.Exchanges on message.GUID equals exchange.ID_Object
                                join routingKey in getMessagesResult.Result.RoutingKeys on message.GUID equals routingKey.ID_Object
                                select new Models.StartModuleMessage
                                {
                                    IdMessage = message.GUID,
                                    NameMessage = message.Name,
                                    IdModuleConfig = config.ID_Other,
                                    NameModuleConfig = config.Name_Other,
                                    IdScheduledJob = scheduler.ID_Other,
                                    NameScheduledJob = scheduler.Name_Other,
                                    IdExchange = exchange.ID_Other,
                                    NameExchange = exchange.Name_Other,
                                    IdRoutingKey = routingKey.ID_Other,
                                    NameRoutingKey = routingKey.Name_Other
                                }).ToList();

                if (!messages.Any())
                {
                    result = Globals.LState_Error.Clone();
                    result.Additional1 = "No messages found!";
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo($"Found {messages.Count} Messages.");

                
                try
                {
                    
                    foreach (var message in messages)
                    {
                        var jsonMessage = Newtonsoft.Json.JsonConvert.SerializeObject(messages);

                        byte[] messageBodyBytes = System.Text.Encoding.UTF8.GetBytes(jsonMessage);

                        IBasicProperties props = request.Connection.Channel.CreateBasicProperties();
                        props.ContentType = "text/plain";
                        props.DeliveryMode = 2;
                        props.Expiration = "36000000";
                        
                        request.Connection.Channel.BasicPublish(message.NameExchange,
                                           message.NameRoutingKey, props,
                                           messageBodyBytes);
                    }
                    
                    
                }
                catch (Exception ex)
                {

                    result = Globals.LState_Error.Clone();
                    result.Additional1 = $"Error while connecting to RabbitMQ: {ex.Message}";
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

                return result;
            });

            

            return taskResult;
        }

        

        public RabbitMQController(Globals globals) : base(globals)
        {
        }
    }
}

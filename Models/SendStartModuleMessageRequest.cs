﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQModule.Models
{
    public class SendStartModuleMessageRequest
    {
        public string IdConfig { get; set; }
        public string PasswordMasterUser { get; set; }

        public RabbitMQConnection Connection { get; set; }

        public IMessageOutput MessageOutput { get; set; }

        public SendStartModuleMessageRequest(string idConfig, string password, RabbitMQConnection connection)
        {
            IdConfig = idConfig;
            PasswordMasterUser = password;
            Connection = connection;
        }
    }
}

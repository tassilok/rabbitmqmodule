﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQModule.Models
{
    public class GetStartModuleMessageModelResult
    {
        public clsOntologyItem BaseMessage { get; set; }
        public List<clsOntologyItem> Messages { get; set; } = new List<clsOntologyItem>();

        public List<clsObjectRel> ModuleConfigs { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> Exchanges { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> Queues { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ScheduledJobs { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> RoutingKeys { get; set; } = new List<clsObjectRel>();
    }
}

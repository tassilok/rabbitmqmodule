﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQModule.Models
{
    public delegate clsOntologyItem ReceivedStartModuleMessageHandler(List<Models.StartModuleMessage> moduleMessage);
    public class RabbitMQConnection : IDisposable
    {
        public event ReceivedStartModuleMessageHandler ReceivedStartModuleMessage;

        private Globals globals;
        public IConnection Connection { get; set; }
        public IModel Channel { get; set; }

        public clsOntologyItem Queue { get; set; }
        public clsOntologyItem Exchange { get; set; }

        public clsOntologyItem RoutingKey { get; set; }

        public ConnectionFactory ConnectionFactory { get; set; }

        private EventingBasicConsumer consumer;
        public EventingBasicConsumer Consumer 
        { 
            get { return consumer; }
            set
            {
                consumer = value;
                consumer.Received += Consumer_Received;
                Channel.BasicConsume(Queue.Name, false, consumer);
            }
        }

        private void Consumer_Received(object chn, BasicDeliverEventArgs ea)
        {
            var body = System.Text.Encoding.UTF8.GetString(ea.Body);
            var message = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.StartModuleMessage[]>(body);
            var receiveResult = ReceivedStartModuleMessage(message.ToList());
            if (receiveResult.GUID == globals.LState_Success.GUID)
            {
                Channel.BasicAck(ea.DeliveryTag, false);
            }
        }

        public clsOntologyItem CheckQueue()
        {
            var result = globals.LState_Success.Clone();
            var basicGetResult = Channel.BasicGet(Queue.Name, false);
            if (basicGetResult == null) return result;
            while (basicGetResult.MessageCount > 0)
            {
                var body = System.Text.Encoding.UTF8.GetString(basicGetResult.Body);
                var messages = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.StartModuleMessage>>(body);
                result = ReceivedStartModuleMessage(messages);
                if (result.GUID == globals.LState_Success.GUID)
                {
                    Channel.BasicAck(basicGetResult.DeliveryTag, true);
                    basicGetResult = Channel.BasicGet(Queue.Name, false);
                }
                else
                {
                    return result;
                }
            }

            return result;
        }

        public RabbitMQConnection(Globals globals)
        {
            this.globals = globals;
        }

        public void Dispose()
        {
            try
            {
                if (Channel != null &&  Channel.IsOpen)
                {
                    Channel.Close();
                }
                if (Connection != null && Connection.IsOpen)
                {
                    Connection.Close();
                }

            }
            catch (Exception ex)
            {

            }
        }
    }
}

﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQModule.Models
{
    public class ConnectionRequest
    {
        public string MasterPassword { get; set; }
        public IMessageOutput MessageOutput { get; set; }

        public ConnectionRequest(string masterPassword)
        {
            MasterPassword = masterPassword;
        }
    }
}

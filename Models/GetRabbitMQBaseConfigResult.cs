﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQModule.Models
{
    public class GetRabbitMQBaseConfigResult
    {
        public clsOntologyItem BaseConfig { get; set; }
        public clsOntologyItem ServerPort { get; set; }

        public clsOntologyItem Server { get; set; }

        public clsOntologyItem PortItem { get; set; }

        public int Port
        {
            get
            {
                var port = 0;
                if (PortItem != null)
                {
                    if (!int.TryParse(PortItem.Name, out port))
                    {
                        port = 0;
                        return port;
                    }
                }

                return port;
            }
        }
        public clsOntologyItem User { get; set; }
        public clsOntologyItem MasterUser { get; set; }

        public clsOntologyItem EmailAddress { get; set; }

        public clsOntologyItem Queue { get; set; }

        public clsOntologyItem Exchange { get; set; }

        public clsOntologyItem RoutingKey { get; set; }
    }
}

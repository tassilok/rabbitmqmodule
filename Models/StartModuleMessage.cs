﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQModule.Models
{
    public class StartModuleMessage
    {
        public string IdMessage { get; set; }
        public string NameMessage { get; set; }
        public string IdModuleConfig { get; set; }
        public string NameModuleConfig { get; set; }

        public string IdScheduledJob { get; set; }
        public string NameScheduledJob { get; set; }

        public string IdExchange { get; set; }
        public string NameExchange { get; set; }

        public string IdRoutingKey { get; set; }
        public string NameRoutingKey { get; set; }
    }
}

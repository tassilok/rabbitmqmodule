﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQModule.Models
{
    public class CreateQueueAndExchangeRequest
    {
        public string PasswordMasterUser { get; set; }

        public RabbitMQConnection Connection { get; set; }

        public IMessageOutput MessageOutput { get; set; }

        public CreateQueueAndExchangeRequest(string password)
        {
            PasswordMasterUser = password;
        }
    }
}
